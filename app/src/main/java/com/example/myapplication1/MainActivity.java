package com.example.myapplication1;

import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication1.database.MyDatabase;
import com.example.myapplication1.database.TblUserData;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity<onCreateOpationsMenu> extends AppCompatActivity {
    EditText etFirstName, etLastName, etEmail;
    Button btnSubmit;
    Button btnSecond;
    ImageView ivClose;
    TextView tvDisplay;

    RadioGroup rgGender;
    RadioButton rbMale;
    RadioButton rbFemale;

    CheckBox chbCricket;
    CheckBox chbFootBall;
    CheckBox chbHockey;

    ImageButton ivCloseName;
    ImageButton ivCloseEmail;
    ImageButton ivClosePhone;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    //    /// Back press two time to exist application
    boolean doubleBackToExitPressedOnce = false;

    //    ImageView ivBackground;
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putString("TextViewValue", tvDisplay.getText().toString());

    }

    //        validation

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new MyDatabase(MainActivity.this).getReadableDatabase();

        setContentView(R.layout.activity_main);
        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActPhoneNumber);
        etEmail = findViewById(R.id.etActEmail);
        ivClose = findViewById(R.id.ivActClose);
        btnSubmit = findViewById(R.id.btnActSubmit);

        tvDisplay = findViewById(R.id.tvActDisplay);

        rgGender = findViewById(R.id.rgActGender);
        rbFemale = findViewById(R.id.rbActFemale);
        rbMale = findViewById(R.id.rbActMale);

        chbCricket = findViewById(R.id.chbActCricket);
        chbFootBall = findViewById(R.id.chbActFootBall);
        chbHockey = findViewById(R.id.chbActHockey);

        etLastName.setText("+91");

        ivCloseEmail = findViewById(R.id.btnActCloseEmail);
        ivCloseName = findViewById(R.id.btnActCloseName);
        ivClosePhone = findViewById(R.id.btnActClosePhoneNo);

//        Button reset= (Button) findViewById(R.id.btnActSubmit);

//      ActionBar
        getSupportActionBar().setTitle(R.string.lbl_login_page);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (rbFemale.isChecked()) {
            chbHockey.setVisibility(View.GONE);
        }
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int i) {

                if (i == R.id.rbActFemale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbCricket.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.GONE);
                }
                if (i == R.id.rbActMale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbCricket.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);
                }
            }
        });


//        btnSecond = findViewById(R.id.btnActSecond);
//    use  this code for assets folder backgroundImage
//        ivBackground =findViewById(R.id.ivActBackground);

//        try {
//            Drawable d = Drawable.createFromStream(getAssets().open("imges/decreased_size.jpg"), null);
//            ivBackground.setImageDrawable(d);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


//        btnSubmit.setOnLogClickListener(new View.OnLogClickListener() {
//            @Override
//            private boolean onClick(View view) {
//                return false;
//
//            }
//        });

//      Editext  clean use the button click
        ivCloseName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.btnActCloseName) ;
                etFirstName.setText("");
            }
        });
        ivClosePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.btnActClosePhoneNo) ;
                etLastName.setText("");
                etLastName.setText("+91");
            }
        });
        ivCloseEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.btnActCloseEmail) ;
                etEmail.setText("");
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etFirstName.setText("");
                etEmail.setText("");
                etLastName.setText("");
                rbFemale.setText("");
                rbMale.setText("");
                chbHockey.setText("");
                chbCricket.setText("");
                chbFootBall.setText("");
            }
        });

//        btnSubmit.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                if (v==btnSubmit) {
//                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
//                }
//            }
//        });

        /*  HashMap Intro  */
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    String name = etFirstName.getText().toString();
                    String phoneNumber = etLastName.getText().toString();
                    String email = etEmail.getText().toString();

                    TblUserData tblUserData = new TblUserData(MainActivity.this);
                    long lastInsertId = tblUserData.insertUserDetail(name, phoneNumber, email);
                    Toast.makeText(getApplicationContext(), lastInsertId > 0 ?
                            "User insert Successfully" : "Something went to wrong ", Toast.LENGTH_SHORT).show();

                  /*  HashMap<String, Object> map = new HashMap<>();
                    map.put(Const.FIRST_NAME, etFirstName.getText().toString());
                    map.put(Const.LAST_NAME, etLastName.getText().toString());
                    map.put(Const.EMAIL_ADDRESS, etEmail.getText().toString());
                    map.put(Const.GENDER, rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString());

                    String hobbies = "";
                    if (chbCricket.isChecked()) {
                        hobbies += "," + chbCricket.getText().toString();
                    }
                    if (chbFootBall.isChecked()) {
                        hobbies += "," + chbFootBall.getText().toString();
                    }
                    if (chbHockey.isChecked()) {
                        hobbies += "," + chbHockey.getText().toString();
                    }

                    map.put(Const.HOBBY, hobbies);
                if(rbFemale.isChecked()){
                    rbFemale.setBackground(Drawable.createFromPath("ic_female_background"));
                }else {
                    rbFemale.setBackground(Drawable.createFromPath("ic_male_background"));
                }
                    userList.add(map);
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    intent.putExtra("UserList", userList);
                    startActivity(intent);*/

        /*        HashMap<String,Integer> map =new HashMap<>();
                map.put("CS",58);
                map.put("Learning",400);
                map.put("Portal",98);
                map.put("GEEKS",1000);
                map.put("For",96);
                map.put("GEEKS",2018);

                Log.d(":1:",""+map.get(58));
                Log.d(":2:",""+map.get("CS"));
                Log.d(":3:",""+map.get(400));
                Log.d(":4:",""+map.get("Learning"));
                Log.d(":5:",""+map.get(1000));
                Log.d(":6:",""+map.get("GEEKS"));  */


//                ArrayList intro
           /*     ArrayList<String>  al= new ArrayList<>();
                al.add("Android");
                al.add("WorShop");
                al.add(1,"For");
                al.add("WorShop");
                al.add("WorShop");
                al.add("Android");

                Log.d("List:::",""+al);
                al.remove("WorShop");
                Log.d("List:::",""+al);
                al.remove(3);
                Log.d("List:::",""+al);
                Log.d("List:::",""+al.get(3));
                Log.d("List:::",""+al.contains("Android"));

                for(int i=0;i<al.size();i++){
                    Log.d("Index["+i+"]","="+al.get(i));
                } */


        /*   ArrayList and HashMap combination
                ArrayList<HashMap<String ,Object>> al1=new ArrayList<>();
                HashMap<String,Object> s1=new HashMap<>();
                s1.put("UserId",1);
                s1.put("FirstName","Keval");
                s1.put("LastName","Jesani");  */


//                explicit intent use second activity enter
               /* Intent intent = new Intent(MainActivity.this,SecondCalculaterActivity.class);
                startActivity(intent); */


//                    Toast.makeText(getApplicationContext(), rbMale.isChecked() ? "Male" : "Female", Toast.LENGTH_LONG).show();

              /*  String firstName = etFirstName.getText().toString();
                String lastName = etLastName.getText().toString();
                String addString = "Mr. Or Ms. Or Mrs. " + firstName + " " + lastName; */

//            Toast.makeText(getApplicationContext(), addString, Toast.LENGTH_LONG).show();
//            /*    tvDisplay.setText(addString); */
                }
            }
        });


//     data translate one activity to second .
     /*   btnSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                String addString = etFirstName.getText().toString() + " " + etLastName.getText().toString();
                intent.putExtra("addString", addString);
                startActivity(intent);
            }
        }); */


//            @Override
//            Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/BalsamiqSans-Bold.ttf");
//        btnSubmit.setTypeface(typeface);
//        tvDisplay.setTypeface(typeface);
//        etLastName.setTypeface(typeface);
//        etFirstName.setTypeface(typeface);


        ivClose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });


        if (savedInstanceState != null) {
            Log.d("BUNDLE VALUE:::", "" + savedInstanceState.toString());
            tvDisplay.setText(savedInstanceState.getString("TextViewValue"));
        }
    }


    Boolean isValid() {
        boolean flag = true;
//
        String email = etEmail.getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z.]+\\.+[a-z]+";
        String phoneNumber = etLastName.getText().toString();
        String name = etFirstName.getText().toString().trim();
        String namePattern = "[A-Za-z\\S]+";
//      Name
        if (TextUtils.isEmpty(etFirstName.getText())) {
            etFirstName.setError(getString(R.string.error_first_name));
            etFirstName.requestFocus(etFirstName.length());
//            etFirstName.requestFocusFromTouch();
//            etFirstName.requestFocus();
            flag = false;
        } else if (!(name.matches(namePattern))) {
            etFirstName.setError("Enter Valid Name");
            etFirstName.requestFocus(etFirstName.length());
//                etFirstName.requestFocusFromTouch();
//                etFirstName.requestFocus();
            flag = false;

        }

//        phone Number

        else if (TextUtils.isEmpty(etLastName.getText())) {
            etLastName.setError("Enter Phone Number");
            etLastName.requestFocus(etLastName.length());
//            etLastName.requestFocusFromTouch();
//            etLastName.requestFocus();
            flag = false;
        } else if (phoneNumber.length() < 10) {
            etLastName.setError("Enter valid phone Number");
            etLastName.requestFocus(etLastName.length());
//                etLastName.requestFocusFromTouch();
//                etLastName.requestFocus();
            flag = false;

        }

//        Email validation

        else if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError(getString(R.string.error_email_address));
            etEmail.requestFocus(etEmail.length());
//            etEmail.requestFocusFromTouch();
//            etEmail.requestFocus();
            flag = false;
        } else if (!(email.matches(emailPattern))) {
            etEmail.setError("Enter Valid Email");
            etEmail.requestFocus(etEmail.length());
//                etEmail.requestFocusFromTouch();
//                etEmail.requestFocus();
            flag = false;
        }


//        Hobbies validation
        if (!(chbCricket.isChecked() || chbFootBall.isChecked() || chbHockey.isChecked())) {
            Toast.makeText(this, "Please select one hobbies", Toast.LENGTH_SHORT).show();
            flag = false;
        }

        String result = "";

        if (chbCricket.isChecked() && (chbFootBall.isChecked() || chbHockey.isChecked())) {
            result += chbCricket.getText().toString() + ",";
        } else if (chbCricket.isChecked()) {
            result += chbCricket.getText().toString() + " ";
        }

        if ((chbFootBall.isChecked() && chbHockey.isChecked())) {
            result += chbFootBall.getText().toString() + ",";
        } else if (chbFootBall.isChecked()) {
            result += chbFootBall.getText().toString() + " ";
        }


        if ((chbHockey.isChecked())) {
            result += chbHockey.getText().toString() + " ";
        }
//            else if(chbHockey.isChecked()){
//                result += chbHockey.getText().toString() + " ";
//            }


        Toast.makeText(this, result, Toast.LENGTH_LONG).show();
        return flag;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

  /*  Button btn=(Button) findViewById(R.id.btnActSubmit);
    btn.setOnClickListener(new View.OnClickListener(){
        public void onClick(View v){
            EditText et=(EditText) findViewById(R.id.btnSubmit);
            et.setText("");
        }
    });*/

    //          menuBar set
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dashboard_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    // Array key
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.new_game) {

        } else if (item.getItemId() == R.id.help) {

        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


}