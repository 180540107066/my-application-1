package com.example.myapplication1;

import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication1.adapter.UserListAdapter;
import com.example.myapplication1.util.Const;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
//    TextView tvDisplay;

    ListView lvUsers;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    UserListAdapter userListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViewReference();
        bindViewValue();
//        Array key back
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


//    MENU ADD

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dashboard_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.new_game) {

        } else if (item.getItemId() == R.id.help) {

        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    void bindViewValue() {
      /*  String value = getIntent().getStringExtra("addString");
        tvDisplay.setText(String.valueOf(value));  */

        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
        userListAdapter = new UserListAdapter(this, userList);
        lvUsers.setAdapter(userListAdapter);

        lvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(LoginActivity.this, userList.get(position).get(Const.GENDER).toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void initViewReference() {
//        tvDisplay = findViewById(R.id.tvActDisplay);

        lvUsers = findViewById(R.id.lvActUserList);
    }


    //    Back press two time to exist application
    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
