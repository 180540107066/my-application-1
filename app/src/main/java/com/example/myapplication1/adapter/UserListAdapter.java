package com.example.myapplication1.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication1.R;
import com.example.myapplication1.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

import static com.example.myapplication1.util.Const.GENDER;
import static java.lang.String.valueOf;

public class UserListAdapter extends BaseAdapter {
    Context context;
    ArrayList<HashMap<String, Object>> userList;
    ImageView img1;


    public UserListAdapter(Context context, ArrayList<HashMap<String, Object>> userList) {
        this.context = context;
        this.userList = userList;
    }


    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null);

        TextView tvName = view1.findViewById(R.id.tvLstName);
        TextView tvEmail = view1.findViewById(R.id.tvLstEmail);
        TextView tvGender = view1.findViewById(R.id.tvLstGender);

        tvName.setText(userList.get(position).get(Const.FIRST_NAME) + " " + userList.get(position).get(Const.LAST_NAME));
        tvEmail.setText(valueOf(userList.get(position).get(Const.EMAIL_ADDRESS)));
        tvGender.setText(valueOf(userList.get(position).get(GENDER)));

        if (valueOf(userList.get(position).get(GENDER)).contains("Female")) {
            tvGender.setBackgroundResource(R.drawable.ic_female_background);
            tvGender.setText("Fe");
        }
        if (valueOf(userList.get(position).get(GENDER)).contains("Male")) {
            tvGender.setBackgroundResource(R.drawable.ic_male_background);
            tvGender.setText("M");
        }

        //        if((Const.GENDER).equals("rbMale")){
//           tvGender.setBackground(Drawable.createFromPath("@drawable/ic_female_background"));
//           tvGender.setText("F");
//          tvGender.setBackground(Drawable.createFromPath("ic_female_background"));
//       }else{
//            tvGender.setBackground(Drawable.createFromPath("@drawable/ic_male_background"));
//        }
    /*    if(tvGender.==rbFemale.isChecked()){
            rbFemale.setBackground(Drawable.createFromPath("ic_female_background"));
        }else {
            rbFemale.setBackground(Drawable.createFromPath("ic_male_background"));
        }*/
        return view1;
    }
}
